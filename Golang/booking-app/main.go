// package main

// import (
// 	"fmt"
// )

// func main() {
// 	// fmt.Print("Hello World")
// 	fmt.Println("Hello World")
// }

// func main() {
// 	fmt.Println("Welcome to our booking application")
// 	fmt.Println("Get your tickets here to attend")
// }

// func main() {
// 	fmt.Println("Welcome to our booking application")
// 	fmt.Println("Get your tickets here to attend")

// 	var conferenceName = "Go Conference"
// 	fmt.Println(conferenceName)
// }

// package main

// import (
// 	"fmt"
// )

// func main() {
// 	var conferenceName = "Go Conference"
// 	const conferenceTickets = 50
// 	var remainingTickets = 50

// 	fmt.Println("Welcome to our", conferenceName, "booking application")
// 	fmt.Println("We have total ", conferenceTickets, "tickets and", remainingTickets, "are still available")
// 	fmt.Println("Get your tickets here to attend")

// }

// Formating Output printf

// package main

// import (
// 	"fmt"
// )

// func main() {
// 	var conferenceName = "Go Conference"
// 	const conferenceTickets = 50
// 	var remainingTickets = 50

// 	fmt.Printf("Welcome to %v booking application", conferenceName)
// 	fmt.Println("We have total ", conferenceTickets, "tickets and", remainingTickets, "are still available")
// 	fmt.Println("Get your tickets here to attend")

// }

// package main

// import (
// 	"fmt"
// )

// func main() {
// 	var conferenceName = "Go Conference"
// 	const conferenceTickets = 50
// 	var remainingTickets = 50

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// }

// Data Types in Go

// package main

// import "fmt"

// func main() {
// 	var conferenceName string = "Go Conference"
// 	const conferenceTickets int = 50
// 	var remainingTickets int = 50

// 	fmt.Printf("conferenceTicket is %T, remainingTickets is %T, conferenceName is %T\n", conferenceTickets, remainingTickets, conferenceName)

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// 	var userName string
// 	var userTicket int
// 	// ask user for their name

// 	userName = "Ton"
// 	userTicket = 2
// 	fmt.Printf("user %v booked %v tickets.\n", userName, userTicket)

// }

// package main

// import "fmt"

// func main() {
// 	conferenceName := "Go Conference"
// 	const conferenceTickets int = 50
// 	var remainingTickets uint = 50

// 	fmt.Printf("conferenceTicket is %T, remainingTickets is %T, conferenceName is %T\n", conferenceTickets, remainingTickets, conferenceName)

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// 	var userName string
// 	var userTicket int
// 	// ask user for their name

// 	userName = "Ton"
// 	userTicket = 2
// 	fmt.Printf("user %v booked %v tickets.\n", userName, userTicket)

// }

// Getting User Input

// package main

// import "fmt"

// func main() {
// 	conferenceName := "Go Conference"
// 	const conferenceTickets int = 50
// 	var remainingTickets uint = 50

// 	fmt.Printf("conferenceTicket is %T, remainingTickets is %T, conferenceName is %T\n", conferenceTickets, remainingTickets, conferenceName)

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// 	var userName string
// 	var userTicket int
// 	// ask user for their name
// 	fmt.Scan(userName)

// 	userTicket = 2
// 	fmt.Printf("user %v booked %v tickets.\n", userName, userTicket)

// }

// package main

// import "fmt"

// func main() {
// 	conferenceName := "Go Conference"
// 	const conferenceTickets int = 50
// 	var remainingTickets uint = 50

// 	fmt.Printf("conferenceTicket is %T, remainingTickets is %T, conferenceName is %T\n", conferenceTickets, remainingTickets, conferenceName)

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// 	var userName string
// 	var userTicket int
// 	// ask user for their name

// 	// tidak dipakai
// 	// fmt.Scan(&userName)

// 	fmt.Println(remainingTickets)
// 	fmt.Println(&remainingTickets)

// 	userTicket = 2
// 	fmt.Printf("user %v booked %v tickets.\n", userName, userTicket)

// }

// package main

// import "fmt"

// func main() {
// 	conferenceName := "Go Conference"
// 	const conferenceTickets int = 50
// 	var remainingTickets uint = 50

// 	fmt.Printf("conferenceTicket is %T, remainingTickets is %T, conferenceName is %T\n", conferenceTickets, remainingTickets, conferenceName)

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// 	var userName string
// 	var userTicket int
// 	// ask user for their name

// 	fmt.Scan(&userName)

// 	// tidak dipakai
// 	// fmt.Println(remainingTickets)
// 	// fmt.Println(&remainingTickets)

// 	userTicket = 2
// 	fmt.Printf("user %v booked %v tickets.\n", userName, userTicket)

// }

// package main

// import "fmt"

// func main() {
// 	conferenceName := "Go Conference"
// 	const conferenceTickets int = 50
// 	var remainingTickets uint = 50

// 	fmt.Printf("conferenceTicket is %T, remainingTickets is %T, conferenceName is %T\n", conferenceTickets, remainingTickets, conferenceName)

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// 	var userName string
// 	var userTicket int
// 	// ask user for their name

// 	fmt.Println("Inter your fisrt name: ")
// 	fmt.Scan(&userName)

// 	// tidak dipakai
// 	// fmt.Println(remainingTickets)
// 	// fmt.Println(&remainingTickets)

// 	userTicket = 2
// 	fmt.Printf("user %v booked %v tickets.\n", userName, userTicket)

// }

// package main

// import "fmt"

// func main() {
// 	conferenceName := "Go Conference"
// 	const conferenceTickets int = 50
// 	var remainingTickets uint = 50

// 	fmt.Printf("conferenceTicket is %T, remainingTickets is %T, conferenceName is %T\n", conferenceTickets, remainingTickets, conferenceName)

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// 	var firstName string
// 	var lastName string
// 	var email string
// 	var userTicket int
// 	// ask user for their name

// 	fmt.Println("Inter your fisrt name: ")
// 	fmt.Scan(&firstName)

// 	fmt.Println("Inter your last name: ")
// 	fmt.Scan(&lastName)

// 	fmt.Println("Inter your email address: ")
// 	fmt.Scan(&email)

// 	fmt.Println("Inter your number tickets: ")
// 	fmt.Scan(&userTicket)

// 	fmt.Printf("Thank you %v %v for booking %v tickets.You will receive a confirmation email at %v\n", firstName, lastName, userTicket, email)

// }

// // Book Ticket Logic
// package main

// import "fmt"

// func main() {
// 	conferenceName := "Go Conference"
// 	const conferenceTickets int = 50
// 	var remainingTickets uint = 50

// 	// fmt.Printf("conferenceTicket is %T, remainingTickets is %T, conferenceName is %T\n", conferenceTickets, remainingTickets, conferenceName)

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// 	var firstName string
// 	var lastName string
// 	var email string
// 	var userTicket uint
// 	// ask user for their name

// 	fmt.Println("Enter your fisrt name: ")
// 	fmt.Scan(&firstName)

// 	fmt.Println("Enter your last name: ")
// 	fmt.Scan(&lastName)

// 	fmt.Println("Enter your email address: ")
// 	fmt.Scan(&email)

// 	fmt.Println("Enter your number tickets: ")
// 	fmt.Scan(&userTicket)

// 	remainingTickets = remainingTickets - userTicket
// 	//harus menggunakan uint, tidak bisa int

// 	fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTicket, email)
// 	fmt.Printf("%v ticket remaining for %v\n", remainingTickets, conferenceName)

// }

// // Arrays
// package main

// import "fmt"

// func main() {
// 	conferenceName := "Go Conference"
// 	const conferenceTickets int = 50
// 	var remainingTickets uint = 50

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// 	var firstName string
// 	var lastName string
// 	var email string
// 	var userTicket uint
// 	// ask user for their name

// 	fmt.Println("Enter your fisrt name: ")
// 	fmt.Scan(&firstName)

// 	fmt.Println("Enter your last name: ")
// 	fmt.Scan(&lastName)

// 	fmt.Println("Enter your email address: ")
// 	fmt.Scan(&email)

// 	fmt.Println("Enter your number tickets: ")
// 	fmt.Scan(&userTicket)

// 	remainingTickets = remainingTickets - userTicket
// 	//harus menggunakan uint, tidak bisa int

// 	var bookings [50]string
// 	bookings[0] = firstName + " " + lastName

// 	fmt.Printf("The whole array: %v\n", bookings)
// 	fmt.Printf("The first array: %v\n", bookings)
// 	fmt.Printf("Array type: %v\n", bookings)
// 	fmt.Printf("Array length: %v\n", len(bookings))

// 	fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTicket, email)
// 	fmt.Printf("%v ticket remaining for %v\n", remainingTickets, conferenceName)

// }

// Slices
// package main

// import "fmt"

// func main() {
// 	conferenceName := "Go Conference"
// 	const conferenceTickets int = 50
// 	var remainingTickets uint = 50
// 	var bookings []string

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// 	var firstName string
// 	var lastName string
// 	var email string
// 	var userTicket uint
// 	// ask user for their name

// 	fmt.Println("Enter your fisrt name: ")
// 	fmt.Scan(&firstName)

// 	fmt.Println("Enter your last name: ")
// 	fmt.Scan(&lastName)

// 	fmt.Println("Enter your email address: ")
// 	fmt.Scan(&email)

// 	fmt.Println("Enter your number tickets: ")
// 	fmt.Scan(&userTicket)

// 	remainingTickets = remainingTickets - userTicket
// 	//harus menggunakan uint, tidak bisa int

// 	bookings = append(bookings, firstName+" "+lastName)

// 	fmt.Printf("The whole slice: %v\n", bookings)
// 	fmt.Printf("The first array: %v\n", bookings[0])
// 	fmt.Printf("Slice type: %v\n", bookings)
// 	fmt.Printf("Slice length: %v\n", len(bookings))

// 	fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTicket, email)
// 	fmt.Printf("%v ticket remaining for %v\n", remainingTickets, conferenceName)

// }

// package main

// import "fmt"

// func main() {
// 	conferenceName := "Go Conference"
// 	const conferenceTickets int = 50
// 	var remainingTickets uint = 50
// 	bookings := []string{}

// 	fmt.Printf("Welcome to %v booking application\n", conferenceName)
// 	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
// 	fmt.Println("Get your tickets here to attend")

// 	var firstName string
// 	var lastName string
// 	var email string
// 	var userTicket uint
// 	// ask user for their name

// 	fmt.Println("Enter your fisrt name: ")
// 	fmt.Scan(&firstName)

// 	fmt.Println("Enter your last name: ")
// 	fmt.Scan(&lastName)

// 	fmt.Println("Enter your email address: ")
// 	fmt.Scan(&email)

// 	fmt.Println("Enter your number tickets: ")
// 	fmt.Scan(&userTicket)

// 	remainingTickets = remainingTickets - userTicket
// 	//harus menggunakan uint, tidak bisa int

// 	bookings = append(bookings, firstName+" "+lastName)

// 	fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTicket, email)
// 	fmt.Printf("%v ticket remaining for %v\n", remainingTickets, conferenceName)

// 	fmt.Printf("These are all our bookings: %v\n", bookings)

// }

// Loops
package main

import "fmt"

func main() {
	conferenceName := "Go Conference"
	const conferenceTickets int = 50
	var remainingTickets uint = 50
	bookings := []string{}

	fmt.Printf("Welcome to %v booking application\n", conferenceName)
	fmt.Printf("We have total of %v tickets and %v are still available\n", conferenceTickets, remainingTickets)
	fmt.Println("Get your tickets here to attend")

	for {
		var firstName string
		var lastName string
		var email string
		var userTicket uint
		// ask user for their name

		fmt.Println("Enter your fisrt name: ")
		fmt.Scan(&firstName)

		fmt.Println("Enter your last name: ")
		fmt.Scan(&lastName)

		fmt.Println("Enter your email address: ")
		fmt.Scan(&email)

		fmt.Println("Enter your number tickets: ")
		fmt.Scan(&userTicket)

		remainingTickets = remainingTickets - userTicket
		//harus menggunakan uint, tidak bisa int
		bookings = append(bookings, firstName+" "+lastName)

		fmt.Printf("Thank you %v %v for booking %v tickets. You will receive a confirmation email at %v\n", firstName, lastName, userTicket, email)
		fmt.Printf("%v ticket remaining for %v\n", remainingTickets, conferenceName)

		fmt.Printf("These are all our bookings: %v\n", bookings)
	}

}

// menit 1:15:21
