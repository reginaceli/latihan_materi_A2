const todos = [
    {
        id:1,
        text: 'Take out trash',
        isCompleted: true
    },
    {
        id:1,
        text: 'Meeting with boss',
        isCompleted: true
    },
    {
        id:1,
        text: 'Dentist appt',
        isCompleted: false
    },
];
    
// console.log(todos[1].text);

// const todoJSON = JSON.stringify(todos);
// console.log(todoJSON);

//For
// for(let i = 0; i < 10; i++){
//     console.log(i); 
// }

//For
for(let i = 0; i <= 10; i++){
        console.log(`For Loop Number: ${i}`);
}

// // while
// let i = 0;
// while(i <10){
//     console.log();
// }

// while
let i = 0;
while(i < 1){
    console.log(`While Loop Number: ${i}`);
    i++; 
}