// Contructor function
// function Person(firstName, lastName, dob){
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.dob = dob;
// }

//Instantance object
// const person1 = new Person('Jhon','Doe','4-3-1980');
// const person2 = new Person('Mary','Saith','3-6-1970');

// console.log(person1.dob.getFullYear());


// Contructor function
// function Person(firstName, lastName, dob){
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.dob =new Date;
//     this.getBirthYear = function(){
//         return this.dob.getFullYear();
//     }

// }

// Instantance object
// const person1 = new Person('Jhon','Doe','4-3-1980');
// const person2 = new Person('Mary','Saith','3-6-1970');

// console.log(person1.dob.getBirthYear());


// Contructor function
// function Person(firstName, lastName, dob){
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.dob =new Date;
//     this.getBirthYear = function(){
//         return this.dob.getFullYear();
//     }
//     this.getFullName = function(){
//         return `${this.firstName} ${this.lastName}`;
//     }
// }

// Instantance object
// const person1 = new Person('Jhon','Doe','4-3-1980');
// const person2 = new Person('Mary','Saith','3-6-1970');

// console.log(person1.dob.getBirthYear());
// console.log(person1.dob.getFullName());


// Contructor function
// function Person(firstName, lastName, dob){
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.dob =new Date;
// }

// Person.prototype.getBirthYear = function(){
//     return this.dob.getFullYear();
// }

// Person.prototype.firstName = function(){
//     return `${this.firstName} ${this.lastName}`;
// }

// //Instantance object
// const person1 = new Person('Jhon','Doe','4-3-1980');
// const person2 = new Person('Mary','Saith','3-6-1970');


// console.log(person2.getFullName());
// console.log(person1);


// Contructor function
function Person(firstName, lastName, dob){
    this.firstName = firstName;
    this.lastName = lastName;
    this.dob =new Date;
}

Person.prototype.getBirthYear = function(){
    return this.dob.getFullYear();
}

Person.prototype.firstName = function(){
    return `${this.firstName} ${this.lastName}`;
}


// Class 
class Person{
    constructor(firstName,lastName, dob){
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob =new Date;
    }
    getBirthYear(){
        return this.dob.getFullYear();
    }
    getFullName(){
        return `${this.firstName} ${this.lastName}`;   
    }
}


//Instantance object
const person1 = new Person('Jhon','Doe','4-3-1980');
const person2 = new Person('Mary','Saith','3-6-1970');


console.log(person2.getFullName());
console.log(person1);
